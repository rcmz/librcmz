#pragma once

#include "vector.hpp"

/*

Fixed size array
Multidimentional

Add the possibility for other ordering than raster ?

*/

namespace rcmz {

template <typename T, int N>
struct Array {
    ivec<N> size;
    T* data;

    Array() = default;
    Array(ivec<N> size) : size(size), data(new T[compProd(size)]) {}
    Array(ivec<N> size, T* data) : size(size), data(data) {}

    // ~Array() { delete[] data; }

    T& operator[](int i) { return data[i]; }
    const T& operator[](int i) const { return data[i]; }

    T& operator[](ivec<N> v) { return data[linearize(size, v)]; }
    const T& operator[](ivec<N> v) const { return data[linearize(size, v)]; }

    void fill(T value) {
        for (int i : range(compProd(size)))
            data[i] = value;
    }
};

template <typename T> using Array1 = Array<T,2>;
template <typename T> using Array2 = Array<T,2>;
template <typename T> using Array3 = Array<T,3>;

}
