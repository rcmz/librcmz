#pragma once

#include "types.hpp"

#include <chrono>

/*

The tick resolution is nanoseconds, so I must use uint64
because a uint32 could only store 4 seconds.

Likewise, I need to use float64 to represent times in seconds
but still have nanosecond accuracy

*/

namespace rcmz::time {
    uint64 getNano();
    uint64 referenceNano = getNano();

    uint64 getNano() {
        return std::chrono::high_resolution_clock::now().time_since_epoch().count() - referenceNano;
    }

    float64 getSecond() {
        return getNano() / 1E9;
    }
}
