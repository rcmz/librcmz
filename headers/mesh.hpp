#pragma once

#include "list.hpp"
#include "image.hpp"

namespace rcmz {

struct Mesh {
    List<vec3> vertices;
    List<vec3> normals;
    List<vec2> texCoords;
    Array2<u8vec3> texture;
};

List<vec3> readSTL(const char* path) {
    FILE* file = fopen(path, "r");
    assert(file);

    char header[80];
    fread(header, sizeof(header), 1, file);

    uint32 triangleCount;
    fread(&triangleCount, sizeof(triangleCount), 1, file);

    List<vec3> vertices(triangleCount*3);

    for (int i : range(0, vertices.size(), 3)) {
        vec3 normal;
        fread(&normal, sizeof(normal), 1, file);

        vec3 positions[3];
        fread(&positions, sizeof(positions), 1, file);
        vertices[i+0] = positions[0];
        vertices[i+1] = positions[1];
        vertices[i+2] = positions[2];

        uint16 attribute;
        fread(&attribute, sizeof(attribute), 1, file);
    }

    fclose(file);
    return vertices;
}

Mesh readOBJ(const char* path) {
    Mesh mesh;

    FILE* file = fopen(path, "r");
    assert(file);

    List<vec3> vertices;
    List<vec3> normals;
    List<vec2> texCoords;

    char line[256];
    while (fgets(line, sizeof(line), file)) {
        vec3 v;
        if(sscanf(line, "v %f %f %f", &v.x, &v.y, &v.z) == 3) {
            vertices.append(v);
            continue;
        }

        vec3 n;
        if(sscanf(line, "vn %f %f %f", &n.x, &n.y, &n.z) == 3) {
            normals.append(n);
            continue;
        }

        vec2 t;
        if(sscanf(line, "vt %f %f", &t.x, &t.y) == 2) {
            t.y = 1 - t.y;
            texCoords.append(t);
            continue;
        }

        ivec3 vi;
        ivec3 ni;
        ivec3 ti;
        if(sscanf(line, "f %u %u %u", &vi.x, &vi.y, &vi.z) == 3) {
            vi -= 1;

            mesh.vertices.append(vertices[vi.x]);
            mesh.vertices.append(vertices[vi.y]);
            mesh.vertices.append(vertices[vi.z]);

            continue;
        }
        if(sscanf(line, "f %u/%u %u/%u %u/%u", &vi.x,&ti.x, &vi.y,&ti.y, &vi.z,&ti.z) == 6) {
            vi -= 1;
            ti -= 1;

            mesh.vertices.append(vertices[vi.x]);
            mesh.vertices.append(vertices[vi.y]);
            mesh.vertices.append(vertices[vi.z]);

            mesh.texCoords.append(texCoords[ti.x]);
            mesh.texCoords.append(texCoords[ti.y]);
            mesh.texCoords.append(texCoords[ti.z]);

            continue;
        }
        if(sscanf(line, "f %u//%u %u//%u %u//%u", &vi.x,&ni.x, &vi.y,&ni.y, &vi.z,&ni.z) == 6) {
            vi -= 1;
            ni -= 1;

            mesh.vertices.append(vertices[vi.x]);
            mesh.vertices.append(vertices[vi.y]);
            mesh.vertices.append(vertices[vi.z]);

            mesh.normals.append(normals[ni.x]);
            mesh.normals.append(normals[ni.y]);
            mesh.normals.append(normals[ni.z]);

            continue;
        }
        if(sscanf(line, "f %u/%u/%u %u/%u/%u %u/%u/%u", &vi.x,&ti.x,&ni.x, &vi.y,&ti.y,&ni.y, &vi.z,&ti.z,&ni.z) == 9) {
            vi -= 1;
            ti -= 1;
            ni -= 1;

            mesh.vertices.append(vertices[vi.x]);
            mesh.vertices.append(vertices[vi.y]);
            mesh.vertices.append(vertices[vi.z]);

            mesh.texCoords.append(texCoords[ti.x]);
            mesh.texCoords.append(texCoords[ti.y]);
            mesh.texCoords.append(texCoords[ti.z]);

            mesh.normals.append(normals[ni.x]);
            mesh.normals.append(normals[ni.y]);
            mesh.normals.append(normals[ni.z]);

            continue;
        }

        char materialPath[256];
        if (sscanf(line, "mtllib %s", materialPath) == 1) {
            FILE* materialFile = fopen(materialPath, "r");
            assert(materialFile);

            char materialLine[256];
            while (fgets(materialLine, sizeof(materialLine), materialFile)) {
                char texturePath[256];
                if (sscanf(materialLine, "map_Kd %s", texturePath) == 1) {
                    mesh.texture = readImage(texturePath);
                    break;
                }
            }

            fclose(materialFile);
            continue;
        }
    }

    fclose(file);
    return mesh;
}

}
