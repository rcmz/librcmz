#pragma once

namespace rcmz {

/*

Uses C++17 fold expressions

*/

void print(auto... args) {
    (std::cout << ... << args) << std::endl;
}

}
