#pragma once

#include "types.hpp"

namespace rcmz {

struct Random {
    uint state;

    Random() {
        state = time::getNano();
    }

    Random(uint seed) {
        state = seed;
    }

    template <typename T>
    T sample();

    template <>
    uint sample() {
        state = state * 747796405u + 2891336453u;
        uint x = ((state >> ((state >> 28u) + 4u)) ^ state) * 277803737u;
        return (x >> 22u) ^ x;
    }

    template <>
    float sample() {
        return float(sample<uint>()) / 4294967296;
    }

    template <>
    vec3 sample() {
        return vec3(sample<float>(), sample<float>(), sample<float>());
    }
};

}
