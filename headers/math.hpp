#pragma once

#include "types.hpp"

#include <math.h>
#include <limits>
#include <algorithm>
#include <initializer_list>

namespace rcmz {

const float PI = M_PI;

constexpr
bool any(auto arg, auto... args) {
    if (arg) return true;
    if constexpr (sizeof...(args)) return any(args...);
    return false;
}

constexpr
bool all(auto arg, auto... args) {
    if (!arg) return false;
    if constexpr (sizeof...(args)) return all(args...);
    return true;
}

constexpr
bool equal(auto value, auto arg, auto... args) {
    if (arg != value) return false;
    if constexpr (sizeof...(args)) return equal(value, args...);
    return true;
}

constexpr
auto min(auto a, auto b) {
    return a < b ? a : b;
}
template <typename T>
constexpr
auto min(T value, auto arg, auto... args) {
    return min(min(value, arg), args...);
}

constexpr
auto max(auto a, auto b) {
    return a > b ? a : b;
}
template <typename T>
constexpr
auto max(T value, auto arg, auto... args) {
    return max(max(value, arg), args...);
}

constexpr
auto clamp(auto value, auto toMin, auto toMax) {
    return min(max(value, toMin), toMax);
}

constexpr
auto mapRange(auto value, decltype(value) fromMin, decltype(value) fromMax, decltype(value) toMin, decltype(value) toMax) {
    return (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
}

constexpr
auto mapRangeClamp(auto value, auto fromMin, auto fromMax, auto toMin, auto toMax) {
    return clamp(mapRange(value, fromMin, fromMax, toMin, toMax), toMin, toMax);
}

constexpr
int sign(auto x) {
    return x < 0 ? -1 : x > 0 ? +1 : 0;
}

// template <typename T, typename U>
// T clamp_cast(U v) {
//     return std::clamp(
//         v,
//         U(std::numeric_limits<T>::min()),
//         U(std::numeric_limits<T>::max())
//     );
// }

constexpr
auto toRadian(auto degree) {
    return degree * (PI/180);
}

constexpr
auto toDegree(auto radian) {
    return radian * (180/PI);
}

template<typename T>
constexpr
void swap(T& a, T& b) {
    // T c = a;
    // a = b;
    // b = c; 
    // which one is actually better ?
    a = a ^ b;
    b = a ^ b;
    a = a ^ b;
}

}
