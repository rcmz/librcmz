#pragma once

#include "vector.hpp"
#include "time.hpp"

#include <GLFW/glfw3.h>

namespace rcmz {

#define RCMZ_WINDOW_KEY(name) name = GLFW_KEY_ ## name

enum struct Key {
    RCMZ_WINDOW_KEY(W),
    RCMZ_WINDOW_KEY(A),
    RCMZ_WINDOW_KEY(S),
    RCMZ_WINDOW_KEY(D),
    RCMZ_WINDOW_KEY(SPACE),
    RCMZ_WINDOW_KEY(LEFT_SHIFT),
};

struct Window {
    GLFWwindow* glfw;
    ivec2 size;

    bool closeRequested;
    float frameDelta;
    float fps;

    vec2 mousePosition;
    vec2 mouseDelta;

    Window(const char* name, ivec2 size, bool vsync = false, bool captureMouse = false) {
        this->size = size;

        glfwInit();
        this->glfw = glfwCreateWindow(this->size.x, this->size.y, name, null, null);
        glfwMakeContextCurrent(this->glfw);

        glfwSwapInterval(vsync ? 1 : 0);
        glfwSetInputMode(this->glfw, GLFW_CURSOR, captureMouse ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);

        update();
    }

    void update(u8vec3* data = null) {
        if (data) {
            glDrawPixels(this->size.x, this->size.y, GL_RGB, GL_UNSIGNED_BYTE, data);
            glfwSwapBuffers(this->glfw);
        }

        glfwPollEvents();

        closeRequested = glfwWindowShouldClose(this->glfw);

        float frameTime = time::getSecond();
        static float previousFrameTime = frameTime;
        frameDelta = frameTime - previousFrameTime;
        previousFrameTime = frameTime;

        fps = 1 / frameDelta;

        dvec2 glfwMousePosition;
        glfwGetCursorPos(this->glfw, &glfwMousePosition.x, &glfwMousePosition.y);
        mousePosition = glfwMousePosition;

        static vec2 previousMousePosition = mousePosition;
        mouseDelta = mousePosition - previousMousePosition;
        previousMousePosition = mousePosition;
    }

    bool isKeyPressed(Key key) {
        return glfwGetKey(this->glfw, static_cast<int>(key)) == GLFW_PRESS;
    }
};

}
