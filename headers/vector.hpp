#pragma once

#include "math.hpp"

#include <iostream>

namespace rcmz {

// generic definition

#define RCMZ_VEC_GENERIC_DEFINITION(N) \
 \
T data[N]; \
 \
constexpr \
T& operator[](int i) { return data[i]; } \
 \
constexpr \
const T operator[](int i) const { return data[i]; } \
 \
constexpr \
Vec() : data{} { \
} \
 \
constexpr \
Vec(T e) : data{} { \
    for (int i = 0; i < N; i++) \
        data[i] = e; \
} \
 \
template <typename U, int M> \
constexpr \
Vec(Vec<U,M> v) : data{} { \
    for (int i = 0; i < min(N, M); i++) \
        data[i] = v[i]; \
} \

template <typename T, int N>
union Vec {
    RCMZ_VEC_GENERIC_DEFINITION(N)
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wnarrowing"
    constexpr
    Vec(auto... args) : data{args...} {}
    #pragma GCC diagnostic pop
};

// size specializations

template <typename T>
union Vec<T,2> {
    RCMZ_VEC_GENERIC_DEFINITION(2)
    struct { T x; T y; };
    struct { T r; T g; };
    constexpr Vec(T x, T y) : data{x, y} {}
};

template <typename T>
union Vec<T,3> {
    RCMZ_VEC_GENERIC_DEFINITION(3)
    struct { T x; T y; T z; };
    struct { Vec<T,2> xy; T _z; };
    struct { T _x; Vec<T,2> yz; };
    struct { T r; T g; T b; };
    constexpr Vec(T x, T y, T z) : data{x, y, z} {}
    constexpr Vec(Vec<T,2> xy, T z) : data{xy.x, xy.y, z} {}
    constexpr Vec(T x, Vec<T,2> yz) : data{x, yz.y, yz.z} {}
};

template <typename T>
union Vec<T,4> {
    RCMZ_VEC_GENERIC_DEFINITION(4)
    struct { T x; T y; T z; T w; };
    struct { Vec<T,2> xy; Vec<T,2> zw; };
    struct { T _x; Vec<T,2> yz; T _w; };
    struct { Vec<T,3> xyz; T _w2; };
    struct { T _x2; Vec<T,3> yzw; };
    struct { T r; T g; T b; T a; };
    constexpr Vec(T x, T y, T z, T w) : data{x, y, z, w} {}
    constexpr Vec(Vec<T,2> xy, T z, T w) : data{xy.x, xy.y, z, w} {}
    constexpr Vec(T x, Vec<T,2> yz, T w) : data{x, yz.y, yz.z, w} {}
    constexpr Vec(T x, T y, Vec<T,2> zw) : data{x, y, zw.z, zw.w} {}
    constexpr Vec(Vec<T,2> xy, Vec<T,2> zw) : data{xy.x, xy.y, zw.z, zw.w} {}
    constexpr Vec(Vec<T,3> xyz, T w) : data{xyz.x, xyz.y, xyz.z, w} {}
    constexpr Vec(T x, Vec<T,3> yzw) : data{x, yzw.y, yzw.z, yzw.w} {}
};

// type specializations

#define RCMZ_VEC_TYPE_SPECIALIZATION(T, prefix) \
template <int N> using prefix ## vec = Vec<T,N>; \
using prefix ## vec2 = Vec<T,2>; \
using prefix ## vec3 = Vec<T,3>; \
using prefix ## vec4 = Vec<T,4>; \

RCMZ_VEC_TYPE_SPECIALIZATION(bool, b)

RCMZ_VEC_TYPE_SPECIALIZATION(float, )
RCMZ_VEC_TYPE_SPECIALIZATION(float, f)
RCMZ_VEC_TYPE_SPECIALIZATION(double, d)
RCMZ_VEC_TYPE_SPECIALIZATION(float16, f16)
RCMZ_VEC_TYPE_SPECIALIZATION(float32, f32)
RCMZ_VEC_TYPE_SPECIALIZATION(float64, f64)

RCMZ_VEC_TYPE_SPECIALIZATION(int, i)
RCMZ_VEC_TYPE_SPECIALIZATION(int8, i8)
RCMZ_VEC_TYPE_SPECIALIZATION(int16, i16)
RCMZ_VEC_TYPE_SPECIALIZATION(int32, i32)
RCMZ_VEC_TYPE_SPECIALIZATION(int64, i64)

RCMZ_VEC_TYPE_SPECIALIZATION(uint, u)
RCMZ_VEC_TYPE_SPECIALIZATION(uint8, u8)
RCMZ_VEC_TYPE_SPECIALIZATION(uint16, u16)
RCMZ_VEC_TYPE_SPECIALIZATION(uint32, u32)
RCMZ_VEC_TYPE_SPECIALIZATION(uint64, u64)

// unary operators
    
#define RCMZ_VEC_UNARY_OP(OP) \
template <typename T, int N> \
constexpr \
Vec<T,N> operator OP (Vec<T,N> v) { \
    Vec<T,N> r; \
    for (int i = 0; i < N; i++) \
        r[i] = OP v[i]; \
    return r; \
} \

RCMZ_VEC_UNARY_OP(+)
RCMZ_VEC_UNARY_OP(-)
RCMZ_VEC_UNARY_OP(!)
RCMZ_VEC_UNARY_OP(~)

// binary operators

#define RCMZ_VEC_BINARY_OP(OP) \
template <typename A, typename B, int N> \
constexpr \
auto operator OP (Vec<A,N> a, Vec<B,N> b) { \
    Vec<decltype(A() OP B()), N> r; \
    for (int i = 0; i < N; i++) \
        r[i] = a[i] OP b[i]; \
    return r; \
} \

RCMZ_VEC_BINARY_OP(+)
RCMZ_VEC_BINARY_OP(-)
RCMZ_VEC_BINARY_OP(*)
RCMZ_VEC_BINARY_OP(/)
RCMZ_VEC_BINARY_OP(%)
RCMZ_VEC_BINARY_OP(&)
RCMZ_VEC_BINARY_OP(|)
RCMZ_VEC_BINARY_OP(^)

// compare operators

#define RCMZ_VEC_COMPARE_OP(OP) \
template <typename A, typename B, int N> \
constexpr \
bool operator OP (Vec<A,N> a, Vec<B,N> b) { \
    for (int i = 0; i < N; i++) \
        if (!(a[i] OP b[i])) \
            return false; \
    return true; \
} \

RCMZ_VEC_COMPARE_OP(==)
RCMZ_VEC_COMPARE_OP(!=)
RCMZ_VEC_COMPARE_OP(>=)
RCMZ_VEC_COMPARE_OP(<=)
RCMZ_VEC_COMPARE_OP(>)
RCMZ_VEC_COMPARE_OP(<)

// promotion for binary and compare operators

#define RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(OP) \
template <typename A, typename B, int N> \
constexpr \
auto operator OP (Vec<A,N> a, B b) { \
    return a OP Vec<B,N>(b); \
} \
template <typename A, typename B, int N> \
constexpr \
auto operator OP (A a, Vec<B,N> b) { \
    return Vec<A,N>(a) OP b; \
} \

RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(+)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(-)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(*)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(/)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(%)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(&)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(|)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(^)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(==)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(!=)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(>=)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(<=)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(>)
RCMZ_VEC_BINARY_AND_COMPARE_OP_PROMOTION(<)

// compare functions

#define RCMZ_VEC_COMPARE_FUNCTION(OP, name) \
template <typename A, typename B, int N> \
constexpr \
bvec<N> name (Vec<A,N> a, Vec<B,N> b) { \
    bvec<N> r; \
    for (int i = 0; i < N; i++) \
        r[i] = a[i] OP b[i]; \
    return r; \
} \
template <typename A, typename B, int N> \
constexpr \
auto name (Vec<A,N> a, B b) { \
    return name (a, Vec<B,N>(b)); \
} \
template <typename A, typename B, int N> \
constexpr \
auto name (A a, Vec<B,N> b) { \
    return name (Vec<A,N>(a), b); \
} \

RCMZ_VEC_COMPARE_FUNCTION(==, equal)
RCMZ_VEC_COMPARE_FUNCTION(!=, notEqual)
RCMZ_VEC_COMPARE_FUNCTION(>=, greaterThanEqual)
RCMZ_VEC_COMPARE_FUNCTION(<=, lessThanEqual)
RCMZ_VEC_COMPARE_FUNCTION(>, greaterThan)
RCMZ_VEC_COMPARE_FUNCTION(<, lessThan)

// compound operators

#define RCMZ_VEC_COMPOUND_OP(OP) \
template <typename T, int N> \
Vec<T,N>& operator OP (Vec<T,N>& a, Vec<T,N> b) { \
    for (int i = 0; i < N; i++) \
        a[i] OP b[i]; \
    return a; \
} \

RCMZ_VEC_COMPOUND_OP(+=)
RCMZ_VEC_COMPOUND_OP(-=)
RCMZ_VEC_COMPOUND_OP(*=)
RCMZ_VEC_COMPOUND_OP(/=)
RCMZ_VEC_COMPOUND_OP(%=)
RCMZ_VEC_COMPOUND_OP(&=)
RCMZ_VEC_COMPOUND_OP(|=)
RCMZ_VEC_COMPOUND_OP(^=)

// promotion for compound operators

#define RCMZ_VEC_COMPOUND_OP_PROMOTION(OP) \
template <typename A, typename B, int N> \
Vec<A,N>& operator OP (Vec<A,N>& a, B b) { \
    return a OP Vec<A,N>(b); \
} \

RCMZ_VEC_COMPOUND_OP_PROMOTION(+=)
RCMZ_VEC_COMPOUND_OP_PROMOTION(-=)
RCMZ_VEC_COMPOUND_OP_PROMOTION(*=)
RCMZ_VEC_COMPOUND_OP_PROMOTION(/=)
RCMZ_VEC_COMPOUND_OP_PROMOTION(%=)
RCMZ_VEC_COMPOUND_OP_PROMOTION(&=)
RCMZ_VEC_COMPOUND_OP_PROMOTION(|=)
RCMZ_VEC_COMPOUND_OP_PROMOTION(^=)

// unary functions

template <typename T, int N>
constexpr
Vec<T,N> abs(Vec<T,N> v) {
    Vec<T,N> r;
    for (int i = 0; i < N; i++)
        r[i] = v[i] < 0 ? -v[i] : v[i];
    return r;
}

template <typename T, int N>
constexpr
T length(Vec<T,N> v) {
    T r = 0;
    for (int i = 0; i < N; i++)
        r += v[i] * v[i];
    return T(sqrt(r));
}

template <typename T, int N>
constexpr
Vec<T,N> normalize(Vec<T,N> v) {
    return v / length(v);
}

template <typename T, uint N>
constexpr
T compSum(Vec<T,N> v) {
    T r = 0;
    for (uint i = 0; i < N; i++)
        r += v[i];
    return r;
}

template <typename T, int N>
constexpr
T compProd(Vec<T,N> v) {
    T r = 1;
    for (int i = 0; i < N; i++)
        r *= v[i];
    return r;
}

template <typename T, int N>
constexpr
bool any(Vec<T,N> v) {
    for (int i = 0; i < N; i++)
        if (v[i])
            return true;
    return false;
}

template <typename T, int N>
constexpr
bool all(Vec<T,N> v) {
    for (int i = 0; i < N; i++)
        if (!v[i])
            return false;
    return true;
}

// binary functions

template <typename A, typename B, int N>
constexpr
auto min(Vec<A,N> a, Vec<B,N> b) {
    Vec<decltype(min(A(), B())), N> r;
    for (int i = 0; i < N; i++)
        r[i] = min(a[i], b[i]);
    return r;
}

template <typename A, typename B, int N>
constexpr
auto max(Vec<A,N> a, Vec<B,N> b) {
    Vec<decltype(max(A(), B())), N> r;
    for (int i = 0; i < N; i++)
        r[i] = max(a[i], b[i]);
    return r;
}

template <typename A, typename B, int N>
constexpr
auto distance(Vec<A,N> a, Vec<B,N> b) {
    return length(a - b);
}

template <typename A, typename B, int N>
constexpr
auto dot(Vec<A,N> a, Vec<B,N> b) {
    decltype(A() * B()) r = 0;
    for (int i = 0; i < N; i++)
        r += a[i] * b[i];
    return r;
}

template <typename A, typename B>
constexpr
auto cross(Vec<A,2> a, Vec<B,2> b) {
    return a.x * b.y - a.y * b.x;
}

template <typename A, typename B>
constexpr
auto cross(Vec<A,3> a, Vec<B,3> b) {
    return Vec<decltype(A() * B()), 3>(
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x
    );
}

template <typename A, typename B>
constexpr
auto reflect(Vec<A,3> i, Vec<B,3> n) {
    return i - 2 * dot(n, i) * n;
}

// promotion for binary functions

#define RCMZ_VEC_BINARY_FUNCTION_PROMOTION(f) \
template <typename A, typename B, int N> \
constexpr \
auto f(Vec<A,N> a, B b) { \
    return f(a, Vec<B,N>(b)); \
} \
template <typename A, typename B, int N> \
constexpr \
auto f(A a, Vec<B,N> b) { \
    return f(Vec<A,N>(a), b); \
} \

RCMZ_VEC_BINARY_FUNCTION_PROMOTION(min)
RCMZ_VEC_BINARY_FUNCTION_PROMOTION(max)

// other functions

template <int N>
int linearize(ivec<N> domain, ivec<N> var) {
    int r = 0;
    int m = 1;
    for (int i = 0; i < N; i++) {
        r += var[i] * m;
        m *= domain[i];
    }
    return r;
}

template <int N>
ivec<N> vectorize(ivec<N> domain, int var) {
    ivec<N> r;
    int m = compProd(domain);
    for (int i = N-1; i >= 0; i--) {
        m /= domain[i];
        r[i] = var / m;
        var -= r[i] * m;
    }
    return r;
}

template <typename T, int N>
std::ostream& operator<<(std::ostream& os, Vec<T,N> v) {
    os << "(";
    for (int i = 0; i < N; i++) {
        os << v[i];
        if (i < N-1) os << ", ";
    }
    os << ")";
    return os;
}

}
