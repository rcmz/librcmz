#pragma once

#include "array.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "external/stb_image.h"

#include <stdio.h>

namespace rcmz {

inline
Array2<u8vec3> readImage(const char* path) {
    int x, y, c;
    uint8* data = stbi_load(path, &x, &y, &c, 3);
    assert(data);
    return Array2<u8vec3>(ivec2(x, y), reinterpret_cast<u8vec3*>(data));
}

inline
void writePPM(const char* path, Array2<u8vec3>& image) {
    FILE* file = fopen(path, "w");
    assert(file);

    fprintf(file, "P6\n%u %u\n255\n", image.size.x, image.size.y);
    fwrite(image.data, sizeof(u8vec3), compProd(image.size), file);

    fclose(file);
}

}
