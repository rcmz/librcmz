#pragma once

#include "vector.hpp"

namespace rcmz {

// generic definition

template <typename T, int N, int M = N>
union Mat {
    // row major
    T data[N*M];
    T data2[N][M];

    constexpr
    T& operator[](int i) { return data[i]; }

    constexpr
    const T operator[](int i) const { return data[i]; }

    constexpr
    T& operator[](int i, int j) { return data2[i][j]; }

    constexpr
    const T operator[](int i, int j) const { return data2[i][j]; }

    constexpr
    Mat() : data{} {}

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wnarrowing"
    constexpr
    Mat(auto... args) : data{args...} {}
    #pragma GCC diagnostic pop

    static constexpr
    Mat cols(auto... args) {
        Mat m;
        for (int j = 0; Vec<T,N> arg : (Vec<T,N>[M]){args...}) {
            for (int i = 0; i < N; i++)
                m.data2[i][j] = arg[i];
            j++;
        }
        return m;
    }
};

// specializations

#define RCMZ_MAT_TYPE_SPECIALIZATION(T, prefix) \
 \
template <int N, int M = N> using prefix ## mat = Mat<T,N,M>; \
 \
using prefix ## mat2 = Mat<T,2>; \
using prefix ## mat3 = Mat<T,3>; \
using prefix ## mat4 = Mat<T,4>; \
 \
using prefix ## mat2x2 = Mat<T,2,2>; \
using prefix ## mat2x3 = Mat<T,2,3>; \
using prefix ## mat2x4 = Mat<T,2,4>; \
 \
using prefix ## mat3x2 = Mat<T,3,2>; \
using prefix ## mat3x3 = Mat<T,3,3>; \
using prefix ## mat3x4 = Mat<T,3,4>; \
 \
using prefix ## mat4x2 = Mat<T,4,2>; \
using prefix ## mat4x3 = Mat<T,4,3>; \
using prefix ## mat4x4 = Mat<T,4,4>; \

RCMZ_MAT_TYPE_SPECIALIZATION(bool, b)

RCMZ_MAT_TYPE_SPECIALIZATION(float, )
RCMZ_MAT_TYPE_SPECIALIZATION(float, f)
RCMZ_MAT_TYPE_SPECIALIZATION(double, d)
RCMZ_MAT_TYPE_SPECIALIZATION(float16, f16)
RCMZ_MAT_TYPE_SPECIALIZATION(float32, f32)
RCMZ_MAT_TYPE_SPECIALIZATION(float64, f64)

RCMZ_MAT_TYPE_SPECIALIZATION(int, i)
RCMZ_MAT_TYPE_SPECIALIZATION(int8, i8)
RCMZ_MAT_TYPE_SPECIALIZATION(int16, i16)
RCMZ_MAT_TYPE_SPECIALIZATION(int32, i32)
RCMZ_MAT_TYPE_SPECIALIZATION(int64, i64)

RCMZ_MAT_TYPE_SPECIALIZATION(uint, u)
RCMZ_MAT_TYPE_SPECIALIZATION(uint8, u8)
RCMZ_MAT_TYPE_SPECIALIZATION(uint16, u16)
RCMZ_MAT_TYPE_SPECIALIZATION(uint32, u32)
RCMZ_MAT_TYPE_SPECIALIZATION(uint64, u64)

// matrix multiplication

template <typename A, typename B, int N, int M, int P>
constexpr
auto operator*(Mat<A,N,P> a, Mat<B,P,M> b) {
    Mat<decltype(A() * B()), N, M> r;
    for (int i = 0; i < N; i++)
    for (int j = 0; j < M; j++)
    for (int k = 0; k < P; k++)
        r[i,j] += a[i,k] * b[k,j];
    return r;
}

template <typename A, typename B, int N, int P>
constexpr
auto operator*(Mat<A,N,P> a, Vec<B,P> b) {
    Vec<decltype(A() * B()), N> r;
    for (int i = 0; i < N; i++)
    for (int k = 0; k < P; k++)
        r[i] += a[i,k] * b[k];
    return r;
}

template <typename A, typename B, int P, int M>
constexpr
auto operator*(Vec<A,P> a, Mat<B,P,M> b) {
    Vec<decltype(A() * B()), M> r;
    for (int j = 0; j < M; j++)
    for (int k = 0; k < P; k++)
        r[j] += a[k] * b[k,j];
    return r;
}

template <typename A, typename B, int N, int M>
constexpr
auto operator*(Mat<A,N,M> a, B b) {
    for (int i = 0; i < N*M; i++)
        a[i] *= b;
    return a;
}

template <typename A, typename B, int N, int M>
constexpr
auto operator*(A a, Mat<B,N,M> b) {
    for (int i = 0; i < N*M; i++)
        b[i] *= a;
    return b;
}

// trace

template <typename T, int N>
constexpr
T trace(Mat<T,N> m) {
    T r = 0;
    for (int i = 0; i < N; i++)
        r += m[i,i];
    return r;
}

// matrix inversion

template <typename T>
constexpr
Mat<T,1> inv(Mat<T,1> m) {
    return Mat<T,1>{1 / m[0]};
}

template <typename T>
constexpr
Mat<T,2> inv(Mat<T,2> m) {
    T det = (m[0] * m[3] - m[1] * m[2]);
    return 1/det * Mat<T,2>{m[3], -m[1], -m[2], m[0]};
}

template <typename T>
constexpr
Mat<T,3> inv(Mat<T,3> m) {
    T a = m[4] * m[8] - m[5] * m[7];
    T b = m[5] * m[6] - m[3] * m[8];
    T c = m[3] * m[7] - m[4] * m[6];
    T d = m[2] * m[7] - m[1] * m[8];
    T e = m[0] * m[8] - m[2] * m[6];
    T f = m[1] * m[6] - m[0] * m[7];
    T g = m[1] * m[5] - m[2] * m[4];
    T h = m[2] * m[3] - m[0] * m[5];
    T i = m[0] * m[4] - m[1] * m[3];
    T det = m[0] * a + m[1] * b + m[3] * c;
    return 1/det * Mat<T,3>(a, d, g, b, e, h, c, f, i);
}

template <typename T>
constexpr
Mat<T,4> inv(Mat<T,4> m) {
    Mat<T,2> a { m[0,0], m[0,1], m[1,0], m[1,1] };
    Mat<T,2> b { m[0,2], m[0,3], m[1,2], m[1,3] };
    Mat<T,2> c { m[2,0], m[2,1], m[3,0], m[3,1] };
    Mat<T,2> d { m[2,2], m[2,3], m[3,2], m[3,3] };

    Mat<T,2> h = inv(d - c * inv(a) * b);
    Mat<T,2> g = -h * c * inv(a);
    Mat<T,2> f = -inv(a) * b * h;
    Mat<T,2> e = inv(a) + inv(a) * b * h * c * inv(a);

    return Mat<T,4> {
        e[0,0], e[0,1], f[0,0], f[0,1],
        e[1,0], e[1,1], f[1,0], f[1,1],
        g[0,0], g[0,1], h[0,0], h[0,1],
        g[1,0], g[1,1], h[1,0], h[1,1]
    };
}

template <typename T, int N>
constexpr
Mat<T,N> inverse(Mat<T,N> m) {
    return inv(m);
}

// unary operators
    
#define RCMZ_MAT_UNARY_OP(OP) \
template <typename T, int N, int M> \
constexpr \
Mat<T,N,M> operator OP (Mat<T,N,M> v) { \
    Mat<T,N,M> r; \
    for (int i = 0; i < N*M; i++) \
        r[i] = OP v[i]; \
    return r; \
} \

RCMZ_MAT_UNARY_OP(+)
RCMZ_MAT_UNARY_OP(-)
RCMZ_MAT_UNARY_OP(!)
RCMZ_MAT_UNARY_OP(~)

// binary operators

#define RCMZ_MAT_BINARY_OP(OP) \
template <typename A, typename B, int N, int M> \
constexpr \
auto operator OP (Mat<A,N,M> a, Mat<B,N,M> b) { \
    Mat<decltype(A() OP B()), N,M> r; \
    for (int i = 0; i < N*M; i++) \
        r[i] = a[i] OP b[i]; \
    return r; \
} \

RCMZ_MAT_BINARY_OP(+)
RCMZ_MAT_BINARY_OP(-)
RCMZ_MAT_BINARY_OP(/)
RCMZ_MAT_BINARY_OP(%)
RCMZ_MAT_BINARY_OP(&)
RCMZ_MAT_BINARY_OP(|)
RCMZ_MAT_BINARY_OP(^)

// compare operators

#define RCMZ_MAT_COMPARE_OP(OP) \
template <typename A, typename B, int N, int M> \
constexpr \
bmat<N,M> operator OP (Mat<A,N,M> a, Mat<B,N,M> b) { \
    bmat<N,M> r; \
    for (int i = 0; i < N*M; i++) \
        r[i] = a[i] OP b[i]; \
    return r; \
} \

// RCMZ_MAT_COMPARE_OP(<)
// RCMZ_MAT_COMPARE_OP(>)
// RCMZ_MAT_COMPARE_OP(<=)
// RCMZ_MAT_COMPARE_OP(>=)
// RCMZ_MAT_COMPARE_OP(==)
// RCMZ_MAT_COMPARE_OP(!=)

// conversion for binary and compare operators

#define RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(OP) \
template <typename A, typename B, int N, int M> \
constexpr \
auto operator OP (Mat<A,N,M> a, B b) { \
    return a OP Mat<B,N,M>(b); \
} \
template <typename A, typename B, int N, int M> \
constexpr \
auto operator OP (A a, Mat<B,N,M> b) { \
    return Mat<A,N,M>(a) OP b; \
} \

RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(+)
RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(-)
RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(/)
RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(%)
RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(&)
RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(|)
RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(^)
// RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(<)
// RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(>)
// RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(<=)
// RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(>=)
// RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(==)
// RCMZ_MAT_BINARY_AND_COMPARE_OP_PROMOTION(!=)

// compound operators

#define RCMZ_MAT_COMPOUND_OP(OP) \
template <typename T, int N, int M> \
Mat<T,N,M>& operator OP (Mat<T,N,M>& a, Mat<T,N,M> b) { \
    for (int i = 0; i < N*M; i++) \
        a[i] OP b[i]; \
    return a; \
} \

RCMZ_MAT_COMPOUND_OP(+=)
RCMZ_MAT_COMPOUND_OP(-=)
RCMZ_MAT_COMPOUND_OP(/=)
RCMZ_MAT_COMPOUND_OP(%=)
RCMZ_MAT_COMPOUND_OP(&=)
RCMZ_MAT_COMPOUND_OP(|=)
RCMZ_MAT_COMPOUND_OP(^=)

// conversion for compound operators

#define RCMZ_MAT_COMPOUND_OP_PROMOTION(OP) \
template <typename A, typename B, int N, int M> \
Mat<A,N,M>& operator OP (Mat<A,N,M>& a, B b) { \
    return a OP Mat<A,N,M>(b); \
} \

RCMZ_MAT_COMPOUND_OP_PROMOTION(+=)
RCMZ_MAT_COMPOUND_OP_PROMOTION(-=)
RCMZ_MAT_COMPOUND_OP_PROMOTION(/=)
RCMZ_MAT_COMPOUND_OP_PROMOTION(%=)
RCMZ_MAT_COMPOUND_OP_PROMOTION(&=)
RCMZ_MAT_COMPOUND_OP_PROMOTION(|=)
RCMZ_MAT_COMPOUND_OP_PROMOTION(^=)

// special constructors

mat4 scalingMat(vec3 scaling) {
    float x = scaling.x;
    float y = scaling.y;
    float z = scaling.z;
    return mat4(
        x, 0, 0, 0,
        0, y, 0, 0,
        0, 0, z, 0,
        0, 0, 0, 1
    );
}

mat4 translationMat(vec3 translation) {
    float x = translation.x;
    float y = translation.y;
    float z = translation.z;
    return mat4(
        1, 0, 0, x,
        0, 1, 0, y,
        0, 0, 1, z,
        0, 0, 0, 1
    );
}

mat4 rotationMatX(float angle) {
    float c = cos(angle);
    float s = sin(angle);
    float m = -s;
    return mat4(
        1, 0, 0, 0,
        0, c, m, 0,
        0, s, c, 0,
        0, 0, 0, 1
    );
}

mat4 rotationMatY(float angle) {
    float c = cos(angle);
    float s = sin(angle);
    float m = -s;
    return mat4(
        c, 0, s, 0,
        0, 1, 0, 0,
        m, 0, c, 0,
        0, 0, 0, 1
    );
}

mat4 rotationMatZ(float angle) {
    float c = cos(angle);
    float s = sin(angle);
    float m = -s;
    return mat4(
        c, m, 0, 0,
        s, c, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    );
}

mat4 perspectiveMat(float fov, float near, float far) {
    float s = 1 / tan(fov / 2);
    float a = far / (far - near);
    float b = -(far * near) / (far - near);
    return mat4(
        s, 0, 0, 0,
        0, s, 0, 0,
        0 ,0, a, b,
        0, 0, 1, 0 
    );
}

// other functions

template <typename T, int N, int M>
std::ostream& operator<<(std::ostream& os, Mat<T,N,M> m) {
    os << "[";
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            os << m[i,j];
            if (j < M-1) os << ", ";
        }
        if (i < N-1) os << "\n ";
    }
    os << "]\n";
    return os;
}

}
