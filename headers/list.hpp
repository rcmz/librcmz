#pragma once

#include "range.hpp"

#include <vector>

/*

Dynamic size array
Just a wrapper around std::vector for now

*/

namespace rcmz {

template <typename T>
struct List : private std::vector<T> {
    List() : std::vector<T>() {}
    List(int size) : std::vector<T>(size) {}
    List(std::initializer_list<T> l) : std::vector<T>(l) {}

    int size() const { return std::vector<T>::size(); }

    T& operator[](int i) { return std::vector<T>::operator[](i); }
    const T& operator[](int i) const { return std::vector<T>::operator[](i); }

    void append(T v) { std::vector<T>::push_back(v); }
    void resize(int size) { std::vector<T>::resize(size); }

    auto begin() { return std::vector<T>::begin(); }
    auto end() { return std::vector<T>::end(); }

    auto data() { return std::vector<T>::data(); }
};

}
