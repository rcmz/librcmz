#pragma once

#include "vector.hpp"

/*

for (init-statement; range-declaration : range-expression)
    loop-statement 

Is just equivalent to

init-statement;
auto && __range = range-expression;
auto __begin = begin-expr;
auto __end = end-expr;
for (; __begin != __end; ++__begin) {
    range-declaration = *__begin;
    loop-statement
}

*/

namespace rcmz {

// scalar

inline
auto range(int start, int stop, int step) {
    struct Iterator {
        int i;
        int step;
        int operator*() { return i; }
        void operator++() { i += step; }
        bool operator!=(int stop) { return i < stop; };
    };
    struct Range {
        int start;
        int stop;
        int step;
        Iterator begin() { return Iterator(start, step); }
        int end() { return stop; }
    };
    return Range(start, stop, step);
}

inline
auto range(int start, int stop) {
    return range(start, stop, 1);
}

inline
auto range(int stop) {
    return range(0, stop, 1);
}

//  vector

template<int N>
inline
auto range(ivec<N> start, ivec<N> stop) {
    struct Iterator {
        ivec<N> start;
        ivec<N> stop;
        ivec<N> i;

        ivec<N> operator*() {
            return i;
        }

        void operator++() {
            for (int n = 0; n < N; n++) {
                i[n]++;
                if (i[n] < stop[n] || n == N-1) return;
                i[n] = start[n];
            }
        }

        bool operator!=(int dummy) {
            return i[N-1] < stop[N-1];
        }
    };

    struct Range {
        ivec<N> start;
        ivec<N> stop;
        Iterator begin() { return Iterator(start, stop, start); }
        int end() { return 0; }
    };

    return Range(start, stop);
}

template<int N>
inline
auto range(ivec<N> stop) {
    return range(ivec<N>(0), stop);
}

}
