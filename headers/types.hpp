#pragma once

#include <cstdint>
#include <sys/types.h>

namespace rcmz {

#define null nullptr

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

// clang does not like float32 and float64

typedef _Float16 float16; 

#ifdef _Float32
typedef _Float32 float32; 
#else
typedef float float32;
#endif

#ifdef _Float64
typedef _Float64 float64; 
#else
typedef double float64;
#endif

}
