#include "rcmz.hpp"
using namespace rcmz;

#include <functional>
#include <optional>

#define safe_delete(p) { if (p) { delete p; p = nullptr; } }

struct Trilean {
    enum : uint8 {
        False,
        True,
        Unknown,
    } value;

    using TrileanValue = decltype(Trilean::Unknown);

    Trilean() : value(Unknown) {}
    Trilean(bool b) : value(b ? True : False) {}
    Trilean(TrileanValue value) : value(value) {}

    explicit operator bool() const {
        if (value == Unknown) throw std::logic_error("Indeterminate trilean value cannot be converted to bool");
        return value == True;
    }

    Trilean operator!() const {
        return
            value == True ? False :
            value == False ? True :
            Unknown;
    }

    Trilean operator&&(const Trilean& other) const {
        return
            value == Unknown || other.value == Unknown ? Unknown :
            value == False || other.value == False ? False :
            True;
    }

    Trilean operator||(const Trilean& other) const {
        return
            value == Unknown || other.value == Unknown ? Unknown :
            value == True || other.value == True ? True :
            False;
    }

    bool operator==(const Trilean& other) const {
        return value == other.value;
    }

    std::string to_string() const {
        return
            value == True ? "True" :
            value == False ? "False" :
            "Unknown";
    }
};

std::ostream& operator<<(std::ostream& os, const Trilean& t) {
    os << t.to_string();
    return os;
}

template<typename T>
struct Averager {
    T value = T();
    int count = 0;

    void append(T v) {
        value += v;
        count++;
    }

    T get() {
        return count ? value / count : T();
    }
};

template<typename T, uint N>
struct NTree {
    bool isLeaf;
    union {
        NTree* children[N];
        T value;
    };

    NTree() : isLeaf(false), children{} {
    };

    NTree(T value) : isLeaf(true), value(value) {
    };

    ~NTree() {
        if (!isLeaf) for (NTree* child : children) safe_delete(child);
    }
};

int main(int argc, char** argv) {
    Array2<u8vec3> image = readImage("tree.png");
    for (ivec2 i : range(image.size / ivec2(1, 2))) {
        swap(image[i], image[ivec2(i.x, image.size.y-1 - i.y)]);
    }

    using QuadTree = NTree<u8vec3, 4>;
    const int levelCount = 8;

    Random random;

    std::function<QuadTree*(int, ivec2)> createTree = [&] (int l, ivec2 p) -> QuadTree* {
        if (l == levelCount-1) {
            u8vec3 value = image[image.size * p / ivec2(1 << l)];
            return value != 0 ? new QuadTree(value) : nullptr;
        } else {
            QuadTree* children[4];
            int counter = 0;
            for (int i : range(4)) {
                children[i] = createTree(l + 1, p * 2 + vectorize(ivec2(2), i));
                if (!children[i]) counter--; else if (children[i]->isLeaf) counter++;
            }
            if (counter == -4) {
                return nullptr;
            } else if (counter == +4) {
                uvec3 value = 0;
                for (int i : range(4)) value += children[i]->value;
                value /= 4;
                return new QuadTree(value);
            } else {
                QuadTree* node = new QuadTree();
                for (int i : range(4)) node->children[i] = children[i];
                return node;
            }
        }
    };
    QuadTree* root = createTree(0, ivec2(0));

    Array2<u8vec3> frameBuffer(1000);
    frameBuffer.fill(0);

    std::function<void(QuadTree*, int, ivec2)> drawTree = [&](QuadTree* node, int l, ivec2 p) {
        if (!node || node->isLeaf) {
            u8vec3 color =
                node ?
                node->value :
                mapRange(random.sample<float>(), 0, 1, 0, 0.1) * 255;
            for (ivec2 s : range(frameBuffer.size * p / ivec2(1 << l), frameBuffer.size * (p + 1) / ivec2(1 << l))) {
                frameBuffer[s] = color;
            } 
        } else {
            for (int i : range(4)) {
                QuadTree* child = node->children[i];
                drawTree(child, l + 1, p * 2 + vectorize(ivec2(2), i));
            }
        }
    }; 
    drawTree(root, 0, ivec2(0));

    Array2<u8vec3> treeFrameBuffer(frameBuffer.size);
    for (int i : range(compProd(frameBuffer.size))) {
        treeFrameBuffer[i] = frameBuffer[i];
    }

    Array1<int> depthBuffer(frameBuffer.size.y);
    int nodeCounter;

    auto resetTest = [&] () {
        for (int i : range(compProd(frameBuffer.size))) {
            frameBuffer[i] = treeFrameBuffer[i];
        }
        depthBuffer.fill(INT_MAX);
        nodeCounter = 0;
    };

    std::function<void(QuadTree*, int, ivec2)> randomTraversal = [&](QuadTree* node, int l, ivec2 p) {
        if (node) {
            nodeCounter++;
            ivec2 start = frameBuffer.size * p / ivec2(1<<l);
            ivec2 end = frameBuffer.size * (p+1) / ivec2(1<<l);
            int depth = start.x;
            bool occluded = true;
            for (int y : range(start.y, end.y)) {
                if (depth < depthBuffer[y]) {
                    occluded = false;
                    break;
                }
            }
            if (occluded) return;
            if (node->isLeaf) {
                for (ivec2 s : range(start, end)) {
                    frameBuffer[s] = 255 * vec3(0,0,1);
                } 
                for (int y : range(start.y, end.y)) {
                    depthBuffer[y] = min(depthBuffer[y], depth);
                }
            } else {
                int is[4];
                for (int j : range(4)) {
                    while (true) {
                        is[j] = random.sample<uint>() % 4;
                        bool failed = false;
                        for (int k : range(j)) {
                            if (is[k] == is[j]) {
                                failed = true;
                                break;
                            }
                        }
                        if (!failed) break;
                    }
                }
                for (int i : is) {
                    QuadTree* child = node->children[i];
                    randomTraversal(child, l + 1, p * 2 + vectorize(ivec2(2), i));
                }
            }
        }
    }; 

    std::function<void(QuadTree*, int, ivec2)> optimalTraversal = [&](QuadTree* node, int l, ivec2 p) {
        if (node) {
            nodeCounter++;
            ivec2 start = frameBuffer.size * p / ivec2(1<<l);
            ivec2 end = frameBuffer.size * (p+1) / ivec2(1<<l);
            int depth = start.x;
            bool occluded = true;
            for (int y : range(start.y, end.y)) {
                if (depth < depthBuffer[y]) {
                    occluded = false;
                    break;
                }
            }
            if (occluded) return;
            if (node->isLeaf) {
                for (ivec2 s : range(start, end)) {
                    frameBuffer[s] = 255 * vec3(0,0,1);
                } 
                for (int y : range(start.y, end.y)) {
                    depthBuffer[y] = min(depthBuffer[y], depth);
                }
            } else {
                for (int i : { 0, 2, 1, 3 }) {
                    QuadTree* child = node->children[i];
                    optimalTraversal(child, l + 1, p * 2 + vectorize(ivec2(2), i));
                }
            }
        }
    }; 

    std::function<void()> rayTraceTraversal = [&]() {
        auto traceRay = [&] (int y) {
            const int treeWidth = 1 << (levelCount-1);
            ivec2 v = ivec2(0, treeWidth * y / frameBuffer.size.y);
            while (v.x < treeWidth) {
                QuadTree* node = root;
                int l = 0;
                while (true) {
                    const int cellWidth = 1 << (levelCount-1 - l);
                    if (!node) {
                        v.x = (v.x / cellWidth + 1) * cellWidth;
                        break;
                    } else {
                        nodeCounter++;
                        if (node->isLeaf) {
                            for (ivec2 s : range(frameBuffer.size * v / treeWidth, frameBuffer.size * (v + 1) / treeWidth)) {
                                frameBuffer[s] = 255 * vec3(1,0,0);
                            }
                            return;
                        } else {
                            ivec2 c = (v / (cellWidth / 2)) % 2;
                            node = node->children[linearize(ivec2(2), c)];
                            l++;
                        }
                    }
                }
            }
        };
        for (int y = 0; y < frameBuffer.size.y; y++) traceRay(y);
    };

    initWindow("quadtree", frameBuffer.size);

    resetTest();
    randomTraversal(root, 0, ivec2(0));
    std::cout << "random: " << nodeCounter << std::endl;

    resetTest();
    optimalTraversal(root, 0, ivec2(0));
    std::cout << "optimal: " << nodeCounter << std::endl;

    resetTest();
    rayTraceTraversal();
    std::cout << "rayTrace: " << nodeCounter << std::endl;
    updateWindow(frameBuffer.data);

    for (int i = 0; !windowShouldClose(); i++) {
        updateWindow();
    }

    return 0;
}
