#include "rcmz.hpp"

using namespace rcmz;

int main(int argc, char** argv) {
    const int N = 10;
    uint64 ticks[N];
    float64 times[N];

    initClock();

    for (int i : range(N))
        ticks[i] = getTick();

    for (int i : range(N))
        times[i] = getTime();

    for (int i : range(N))
        std::cout << ticks[i] << " " << times[i] << std::endl;

    for (int i : range(1, N))
        std::cout << ticks[i] - ticks[i-1] << " " << times[i] - times[i-1] << std::endl;

    return 0;
}
