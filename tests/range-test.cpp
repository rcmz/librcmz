#include "rcmz.hpp"
#include <chrono>

using namespace rcmz;

#define N 1000000

/*

range based for loop can be just as fast as regular foor loops, if not faster !
(in release mode, not in debug)

clang is much faster and much smarter than gcc in all cases

*/

int main(int argc, char** argv) {
    for (auto v : range(ivec3(1, 2, 3)))
        std::cout << v << std::endl;

    for (int t : range(3)) {
        int seed = getTick();

        auto start = getTick();
        {
            int c = seed;
            for (int i = 0; i < N; i++)
                c *= i;
            std::cout << c << std::endl;
        }
        auto mid = getTick();
        {
            int c = seed;
            for (int i : range(N))
                c *= i;
            std::cout << c << std::endl;
        }
        auto end = getTick();

        std::cout << "regular for loop took\t" << mid - start << std::endl;
        std::cout << "range for loop took\t" << end - mid << std::endl;
    }

    return 0;
}
