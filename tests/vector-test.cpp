#include "vector.hpp"

#include <chrono>

using namespace rcmz;

constexpr auto c = Vec<int, 3>(5);
constexpr auto d = Vec<int, 3>();
int array[c[0]];
// int array2[(c + d)[1]];
// int array3[(c + 1)[1]];
// int array4[(1 + c)[1]];

constexpr auto e = Vec<float, 3>(1, 2, 3);
constexpr auto e5 = Vec<float, 5>(1, 2, 3, 4, 5);

int main(int argc, char** argv) {
    // A<0> a0;
    // sizeof(a0);

    // A<1> a1;
    // sizeof(a1);

    // A a;
    // sizeof(a);

    // B b;
    // sizeof(b);
    // // b.

    // return 0;

    Vec<int, 2> a;
    a[0] = 1;
    a[1] = 2;

    Vec<int, 2> b;
    b[0] = 3;
    b[1] = 4;

    std::cout << a + b << std::endl;
    std::cout << a + 1.0f << std::endl;
    std::cout << 1.0f + a << std::endl;
    std::cout << (a == 1) << std::endl;

    a += b;
    a += 1.0f;
    a = b;
    a += Vec<float, 2>();

    auto comp1 = a == Vec<float, 2>();
    auto comp2 = a == 1;
    auto comp3 = 1.0f == a;

    auto r1 = Vec<float, 3>() + Vec<float, 3>();
    auto r2 = Vec<float, 3>() + Vec<int, 3>();
    auto r3 = Vec<int, 3>() + Vec<float, 3>();

    auto r4 = Vec<int, 3>() + 1.0f;
    auto r5 = 1.0f + Vec<int, 3>();

    auto r6 = Vec<float, 2>(a);
    auto r7 = Vec(a);

    std::cout << a << std::endl;

    ivec3 i3 = ivec3(1, 2, 3);
    vec3 f3;
    dvec3 d3;
    auto r8 = distance(i3, f3);
    auto r9 = length(i3);
    auto r10 = length(d3);
    auto r11 = -f3;

    auto r12 = f16vec3(1, 2, 3) * u64vec3(4, 5, 6);

    // typedef Vec<float, 4> V;
    typedef vec4 V;
    const size_t M = 10000;

    auto *A = new V[M];
    auto *B = new V[M];
    auto *C = new V[M];

    auto startTimepoint = std::chrono::high_resolution_clock::now();

    for (int j = 0; j < 10000; j++)
    for (int i = 0; i < M; i++)
        C[i] += A[i] + B[i];

    auto endTimepoint = std::chrono::high_resolution_clock::now();
    auto duration = (endTimepoint - startTimepoint).count();
    std::cout << duration / 1000000000.0f << std::endl;

    return 0;
}
