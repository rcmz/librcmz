#include <functional>

#include "rcmz.hpp"
using namespace rcmz;

vec3 textureAccessFiltered(const Array2<u8vec3>& texture, vec2 texCoord) {
    vec2 fragPos = texCoord * (texture.size - 1);
    vec3 fragColor = 0;
    for (ivec2 j : range(ivec2(2))) {
        ivec2 texPos = ivec2(fragPos) + j;
        vec3 texColor = texture[texPos] / 255.f;
        float texFactor = compProd(max(1 - abs(fragPos - texPos), 0));
        fragColor += texColor * texFactor;
    }
    return fragColor;
}

template <int N>
vec<N> interpolate(vec<N> a0, vec<N> a1, vec<N> a2, vec3 bc) {
    return mat<N,3>(a0, a1, a2) * bc;
}

template <int N>
vec<N> interpolate(vec<N> a0, vec<N> a1, vec<N> a2, vec3 bc, vec3 ws) {
    return mat<N,3>::cols(a0 / ws[0], a1 / ws[1], a2 / ws[2]) * bc / dot(1 / ws, bc);
}

template <
    typename VertexAttribute, typename FrameBuffer,
    typename VertexUniform, typename FragmentUniform,
    typename VertexOutput, typename FragmentOutput
> void rasterizationPipeline(
    List<VertexAttribute>& attributes, FrameBuffer& frameBuffer,
    void(*vertexShader)(const VertexUniform&, const VertexAttribute&, vec4&, VertexOutput&),
    void(*fragmentShader)(const FragmentUniform&, vec4, const VertexOutput&, FragmentOutput&),
    VertexUniform& vertexUniform, FragmentUniform& fragmentUniform
) {
    for (int i : range(0, attributes.size(), 3)) {
        vec4 positions[3];
        VertexOutput vertexOutputs[3];

        for (int j : range(3)) {
            vertexShader(vertexUniform, attributes[i+j], positions[j], vertexOutputs[j]);
            positions[j].xyz /= positions[j].w;
            positions[j].xy = mapRange(positions[j].xy, -1, +1, 0, frameBuffer.size-1);
        }

        if (positions[0].w <= 0 || positions[1].w <= 0 || positions[2].w <= 0) continue;

        vec4 v0 = positions[0];
        vec4 v1 = positions[1];
        vec4 v2 = positions[2];

        ivec2 bboxMin = clamp(min(v0, v1, v2).xy, 0, frameBuffer.size-1);
        ivec2 bboxMax = clamp(max(v0, v1, v2).xy+1, 0, frameBuffer.size-1);

        for (ivec2 p : range(bboxMin, bboxMax)) {
            vec2 fc = p + 0.5;

            auto side = [&](vec2 e0, vec2 e1) { return cross(fc - e0, e1 - e0); };
            vec3 sides = vec3(side(v0.xy, v1.xy), side(v1.xy, v2.xy), side(v2.xy, v0.xy));
            // if (!(sides >= 0 || sides <= 0)) continue;
            if (!(sides >= 0)) continue;

            vec3 bc(
                cross(v1.xy - fc, v2.xy - fc) / cross(v1.xy - v0.xy, v2.xy - v0.xy),
                cross(v2.xy - fc, v0.xy - fc) / cross(v1.xy - v0.xy, v2.xy - v0.xy),
                cross(v0.xy - fc, v1.xy - fc) / cross(v1.xy - v0.xy, v2.xy - v0.xy)
            );

            vec3 ws { v0.w, v1.w, v2.w };

            vec4 fragCoord = interpolate(v0, v1, v2, bc, ws);
            fragCoord.w = 1 / fragCoord.w;

            float depth = fragCoord.z;
            if (depth < 0 || depth > 1 || depth >= frameBuffer.depth[p]) continue;
            frameBuffer.depth[p] = depth;

            VertexOutput interpolatedVertexOuput;
            interpolatedVertexOuput.normal = interpolate(vertexOutputs[0].normal, vertexOutputs[1].normal, vertexOutputs[2].normal, bc, ws);
            interpolatedVertexOuput.texCoord = interpolate(vertexOutputs[0].texCoord, vertexOutputs[1].texCoord, vertexOutputs[2].texCoord, bc, ws);

            FragmentOutput fragmentOutput;
            fragmentShader(fragmentUniform, fragCoord, interpolatedVertexOuput, fragmentOutput);
            frameBuffer.color[p] = clamp(fragmentOutput.color, 0, 1) * 255.f;
        }
    }
}

void rasterizeTriangles(Array2<u8vec3>& colorBuffer, Array2<float>& depthBuffer, Mesh& mesh, mat4 objectMat, mat4 viewMat, mat4 projectionMat) {
    List<vec4> vertices(mesh.vertices.size());

    for (int i : range(vertices.size())) {
        vec4& v = vertices[i];
        v = projectionMat * viewMat * objectMat * vec4(mesh.vertices[i], 1);
        v.xyz /= v.w;
        v.xy = mapRange(v.xy, -1, +1, 0, colorBuffer.size-1);
    }

    for (int i : range(0, vertices.size(), 3)) {
        if (vertices[i+0].w <= 0 || vertices[i+1].w <= 0 || vertices[i+2].w <= 0) continue;

        vec4 v0 = vertices[i+0];
        vec4 v1 = vertices[i+1];
        vec4 v2 = vertices[i+2];

        ivec2 bboxMin = clamp(min(v0, v1, v2).xy, 0, colorBuffer.size-1);
        ivec2 bboxMax = clamp(max(v0, v1, v2).xy+1, 0, colorBuffer.size-1);

        for (ivec2 p : range(bboxMin, bboxMax)) {
            vec2 fc = p + 0.5;

            auto side = [&](vec2 e0, vec2 e1) -> int { return sign(cross(fc - e0, e1 - e0)); };
            ivec3 sides = ivec3(side(v0.xy, v1.xy), side(v1.xy, v2.xy), side(v2.xy, v0.xy));
            // if (!(sides >= 0 || sides <= 0)) continue;
            if (!(sides >= 0)) continue;

            vec3 bc = vec3(
                cross(v1.xy - fc, v2.xy - fc) / cross(v1.xy - v0.xy, v2.xy - v0.xy),
                cross(v2.xy - fc, v0.xy - fc) / cross(v1.xy - v0.xy, v2.xy - v0.xy),
                cross(v0.xy - fc, v1.xy - fc) / cross(v1.xy - v0.xy, v2.xy - v0.xy)
            );

            float depth = dot(bc, vec3(v0.z, v1.z, v2.z));
            if (depth < 0 || depth > 1 || depth >= depthBuffer[p]) continue;
            depthBuffer[p] = depth;

            // affine interpolation
            // auto interpolate = [&](auto& values) {
            //     return mat3(values[i+0], values[i+1], values[i+2]) * bc;
            // };

            // perspective correct interpolation
            auto interpolate = [&](auto& values) {
                return mat3::cols(values[i+0] / v0.w, values[i+1] / v1.w, values[i+2] / v2.w) * bc / dot(1 / vec3(v0.w, v1.w, v2.w), bc);
            };

            vec3 position = interpolate(mesh.vertices);
            position = vec3(viewMat * objectMat * vec4(position, 1));

            // vec3 normal = cross(mesh.vertices[i+1] - mesh.vertices[i+0], mesh.vertices[i+2] - mesh.vertices[i+0]);
            vec3 normal = interpolate(mesh.normals);
            normal = normalize(vec3(viewMat * objectMat * vec4(normal, 0)));

            vec2 texCoord = interpolate(mesh.texCoords);

            // vec3 albedo = mesh.texture[ivec2(texCoord * mesh.texture.size)] / 255.f;
            vec3 albedo = textureAccessFiltered(mesh.texture, texCoord);

            // vec3 radiance = 0;

            // struct Light {
            //     vec3 position;
            //     vec3 color;
            // } lights[] {
            //     { .position = vec3(-10, 0, 0), .color = vec3(1, 0, 0) },
            //     { .position = vec3(+10, 0, 0), .color = vec3(0, 1, 0) },
            //     { .position = vec3(0, +10, 0), .color = vec3(0, 0, 1) }
            // };

            // for (Light light : lights) {
            //     vec3 lightPosition = vec3(viewMat * vec4(light.position, 1));
            //     vec3 lightVector = normalize(lightPosition - position);
            //     float diffuse = max(dot(normal, lightVector), 0);

            //     vec3 reflectedLightVector = normalize(reflect(-lightVector, normal));
            //     vec3 viewVector = normalize(vec3(0) - position);
            //     float specular = pow(max(dot(reflectedLightVector, viewVector), 0), 100);

            //     radiance += light.color * (diffuse * 0.5 + specular * 0.5);
            // }

            vec3  color = 1;
            color *= albedo;
            // color *= radiance;

            // color *= normal;
            // color *= Random(i).sample<vec3>();

            colorBuffer[p] = clamp(color, 0, 1) * 255.f;
        }
    }
}

mat4 getViewMat(Window& window) {
    static vec2 rotationAngles = vec2(0);
    rotationAngles += window.mouseDelta * 0.002;        
    mat4 rotation = rotationMatY(rotationAngles.x) * rotationMatX(rotationAngles.y);

    vec3 positionDelta(
        window.isKeyPressed(Key::D) - window.isKeyPressed(Key::A),
        window.isKeyPressed(Key::SPACE) - window.isKeyPressed(Key::LEFT_SHIFT),
        window.isKeyPressed(Key::W) - window.isKeyPressed(Key::S)
    );
    if (length(positionDelta)) positionDelta = normalize(positionDelta);
    positionDelta = vec3(rotation * vec4(positionDelta, 0));

    static vec3 position = vec3(0, 0, -3);
    position += positionDelta * window.frameDelta * 3;

    static vec3 smoothPosition = position;
    smoothPosition += (position - smoothPosition) * min(window.frameDelta * 10, 1);

    mat4 translation = translationMat(smoothPosition); 

    return inverse(translation * rotation);
}

Mesh createCheckerBoardMesh() {
    Mesh mesh;

    mesh.texture = Array2<u8vec3>(ivec2(20));
    for (ivec2 p : range(mesh.texture.size)) {
        mesh.texture[p] = (p.x % 2) ^ (p.y % 2) ? 255 : 0;
    }

    mesh.texCoords.append(vec2(0, 0));
    mesh.texCoords.append(vec2(1, 0));
    mesh.texCoords.append(vec2(1, 1));

    mesh.texCoords.append(vec2(0, 0));
    mesh.texCoords.append(vec2(1, 1));
    mesh.texCoords.append(vec2(0, 1));

    for (vec2 texCoord : mesh.texCoords) {
        mesh.vertices.append(vec3(texCoord * 2 - 1, 0));
        mesh.normals.append(vec3(0, 0, 1));
    }

    return mesh;
}

Mesh createToonShadingMesh(const Mesh& mesh, float scale) {
    Mesh toonMesh;

    toonMesh.texture = Array2<u8vec3>(ivec2(1));
    toonMesh.texture[0] = 0;

    for (int i : range(mesh.vertices.size())) {
        i += (int[]){ 0, +1, -1 }[i % 3]; 

        toonMesh.vertices.append(mesh.vertices[i] + mesh.normals[i] * scale);
        toonMesh.normals.append(vec3(0));
        toonMesh.texCoords.append(vec2(0));
    }

    return toonMesh;
}

int main(int argc, char** argv) {
    Array2<u8vec3> windowBuffer(ivec2(1000));
    Window window("rasterizer", windowBuffer.size, false, true);

    Mesh mesh = readOBJ("spot.obj");
    // Mesh mesh = createCheckerBoardMesh();
    // Mesh toonMesh = createToonShadingMesh(mesh, 0.02);

    struct VertexAttribute {
        vec3 position;
        vec3 normal;
        vec2 texCoord;
    };

    List<VertexAttribute> attributes(mesh.vertices.size());
    for (int i : range(mesh.vertices.size())) {
        attributes[i].position = mesh.vertices[i];
        attributes[i].normal = mesh.normals[i];
        attributes[i].texCoord = mesh.texCoords[i];
    }

    Array2<u8vec3> colorBuffer(windowBuffer.size);
    Array2<float> depthBuffer(colorBuffer.size);

    struct FrameBuffer {
        ivec2 size;
        Array2<u8vec3>& color;
        Array2<float>& depth;
    };

    FrameBuffer frameBuffer {
        .size = colorBuffer.size,
        .color = colorBuffer,
        .depth = depthBuffer,
    };

    struct VertexUniform {
        const mat4& objectMat;
        const mat4& viewMat;
        const mat4& projMat;
    };

    struct VertexOutput {
        vec3 normal;
        vec2 texCoord;
    };

    void(*vertexShader)(const VertexUniform&, const VertexAttribute&, vec4&, VertexOutput&) =
    [] (const VertexUniform& uniform, const VertexAttribute& attribute, vec4& position, VertexOutput& vertexOutput) {
        position = uniform.projMat * uniform.viewMat * uniform.objectMat * vec4(attribute.position, 1);
        vertexOutput.normal = uniform.viewMat * uniform.objectMat * vec4(attribute.normal, 0);
        vertexOutput.texCoord = attribute.texCoord;
    };

    struct FragmentUniform {
        const Array2<u8vec3>& albedoTexture;
    };

    struct FragmentOutput {
        vec3 color;
    };

    void(*fragmentShader)(const FragmentUniform&, vec4, const VertexOutput&, FragmentOutput&) =
    [] (const FragmentUniform& uniform, const vec4 fragCoord, const VertexOutput& vertexOutput, FragmentOutput& fragmentOuput) {
        // fragmentOuput.color = fragCoord.z;
        // fragmentOuput.color = uniform.albedoTexture[ivec2(vertexOutput.texCoord * uniform.albedoTexture.size)] / 255.f;
        fragmentOuput.color = textureAccessFiltered(uniform.albedoTexture, vertexOutput.texCoord);
    };

    while (!window.closeRequested) {
        mat4 objectMat = rotationMatY(PI);
        // mat4 objectMat = rotationMatY(time::getSecond());
        mat4 viewMat = getViewMat(window);
        mat4 projMat = perspectiveMat(toRadian(60), 0.1, 10);

        VertexUniform vertexUniform {
            .objectMat = objectMat,
            .viewMat = viewMat,
            .projMat = projMat,
        };

        FragmentUniform fragmentUniform {
            .albedoTexture = mesh.texture,
        };

        colorBuffer.fill(u8vec3(180, 230, 230));
        depthBuffer.fill(1);

        rasterizationPipeline(attributes, frameBuffer, vertexShader, fragmentShader, vertexUniform, fragmentUniform);

        // rasterizeTriangles(colorBuffer, depthBuffer, mesh, objectMat, viewMat, projMat);
        // rasterizeTriangles(colorBuffer, depthBuffer, toonMesh, objectMat, viewMat, projMat);

        for (ivec2 p : range(windowBuffer.size)) {
            windowBuffer[p] = colorBuffer[p * (frameBuffer.size-1) / (windowBuffer.size-1)];
        }

        window.update(windowBuffer.data);

        print(window.fps, " fps @ ", colorBuffer.size.x, "x", colorBuffer.size.y);

        auto dynamicResSmoothing = [](float fps) -> float {
            const float target = 60;
            const float margin = 10;
            float delta = fps - target;
            if (delta < 0) return delta;
            else if (delta < margin) return 0;
            else return delta - margin;
        };

        frameBuffer.size += dynamicResSmoothing(window.fps);
        frameBuffer.size = clamp(frameBuffer.size, ivec2(100), windowBuffer.size);
    }
}
